`timescale 1us/100ns


module toplevel_test;
    logic clock;
    logic reset;
    logic start;
    logic signed [2:0][31:0] in;
    logic finish;
    logic signed [3:0][15:0] out_r, out_g, out_b;
	logic send_start;
	logic [1:0] cycle_count_pre, input_count_pre, cycle_count, input_count;
	toplevel dut_top(
    .clock,
    .reset,
    .start,
    .in ,
    .out_r, .out_g, .out_b,
    .finish //.send_start,
	//.cycle_count, .input_count
	);


    always begin
    	#1.2;
        clock=~clock;
    end
	
	int data_file1,data_file2,data_file3,data_file4, out_file1, out_file2, out_file3, out_file4; //file descriptors
	logic [5:0] count;
	logic [95:0] x;
	logic [2:0] in_count;
	initial begin
		$dumpfile("toplevel_test.vcd");
		$dumpvars(0, toplevel_test.dut_top);

		clock = 0;
		x = 0;

		out_file1 = $fopen("./testbench/output_r.txt", "w");
		out_file2 = $fopen("./testbench/output_g.txt", "w");
		out_file3 = $fopen("./testbench/output_b.txt", "w");
		count = 0;
		in_count = 0;
		reset = 1'b1;
		in[0] = 0; in[1] = 0; in[2] = 0;
		start = 0;
		@(posedge clock);
		@(posedge clock);
		@(posedge clock);
		@(posedge clock);
		reset = 1'b0;
		in[0] = 0; in[1] = 0; in[2] = 0;
		@(posedge clock);

		data_file1 = $fopen("./testbench/input_pattern_fid.txt", "r");
		while (! $feof(data_file1)) begin //read until an "end of file" is reached.
			//$fscanf(data_file1,"%h\n",x); //scan each line and get the value as an hexadecimal
		
			$fscanf(data_file1,"%h\n",x); //scan each line and get the value as an hexadecimal
			in[0] = x[95:64]; in[1] = x[63:32]; in[2] = x[31:0]; start = 1'b1; 
			
			if (count == 0  | count == 5) begin
				count = 0;
			end
			else begin
				$fwrite(out_file1,"%h\n",out_r[count-1]);
				$fwrite(out_file2,"%h\n",out_g[count-1]);
				$fwrite(out_file3,"%h\n",out_b[count-1]);
				count = count+1;
			end
			if (finish == 1'b1) begin
				count = 1;
			end
			@(posedge clock);
		end


		if (finish == 1'b1) begin
			@(posedge clock);
			$fwrite(out_file1,"%h\n",out_r[0]);$fwrite(out_file2,"%h\n",out_g[0]);$fwrite(out_file3,"%h\n",out_b[0]); @(posedge clock);
			$fwrite(out_file1,"%h\n",out_r[1]);$fwrite(out_file2,"%h\n",out_g[1]);$fwrite(out_file3,"%h\n",out_b[1]); @(posedge clock);
			$fwrite(out_file1,"%h\n",out_r[2]);$fwrite(out_file2,"%h\n",out_g[2]);$fwrite(out_file3,"%h\n",out_b[2]);@(posedge clock);
			$fwrite(out_file1,"%h\n",out_r[3]);$fwrite(out_file2,"%h\n",out_g[3]);$fwrite(out_file3,"%h\n",out_b[3]); @(posedge clock);
		end
		else begin
			@(posedge clock);
		end

/*
		while (! $feof(data_file1)) begin //read until an "end of file" is reached.
			//$fscanf(data_file1,"%h\n",x); //scan each line and get the value as an hexadecimal
			if (count == 0) begin
				$fscanf(data_file1,"%h\n",x); //scan each line and get the value as an hexadecimal
				if (in_count != 3) begin
					start = 1'b0; 
					count = count;
					in[in_count][2] = x[95:64]; in[in_count][1] = x[63:32]; in[in_count][2] = x[31:0];
					in_count = in_count+1;
				end
				else begin
					in[2][2] = x[95:64]; in[2][1] = x[63:32]; in[2][2] = x[31:0];
					start = 1'b1; count = count+1; in_count = 0;
					@(posedge clock);
				end
			end
			else begin
				$fscanf(data_file1,"%h\n",x); //scan each line and get the value as an hexadecimal
				if (in_count != 3) begin
					start = 1'b0; 
					in[in_count][2] = x[95:64]; in[in_count][1] = x[63:32]; in[in_count][2] = x[31:0];
					in_count = in_count+1; count = count;
				end
				else begin
					in[2][2] = x[95:64]; in[2][1] = x[63:32]; in[2][2] = x[31:0];
					start = 1'b0; count = count+1; in_count = 0;
					@(posedge clock);
				end
			end
			
		end
		$fwrite(out_file1,"%h\n",out[0]); @(posedge clock);
		$fwrite(out_file1,"%h\n",out[1]); @(posedge clock);
		$fwrite(out_file1,"%h\n",out[2]); @(posedge clock);
		$fwrite(out_file1,"%h\n",out[3]); @(posedge clock);
*/
			
/*
		for (int i = 1; i < 56; i++) begin
			in[0][0] = i <<< 16; in[0][1] = i <<< 16; in[0][2] = i <<< 16; start = 0;
			@(posedge clock);
		end

		in[0][0] = 32'd0; in[0][1] = 32'd0; in[0][2] = 32'd0; in[1][0] = 32'd1; in[1][1] = 32'd0; in[1][2] = 32'd0; in[2][0] = 32'd0; in[2][1] = 32'd0; in[2][2] = 32'd0;
		@(posedge clock);
		for (int i = 1; i < 56; i++) begin
			in[0][0] = i <<< 16; in[0][1] = i <<< 16; in[0][2] = i <<< 16; start = 0;
			@(posedge clock);
		end
		in[0][0] = 0; in[0][1] = 0; in[0][2] = 0;
		@(posedge clock);
		for (int i = 1; i < 56; i++) begin
			in[0][0] = i <<< 16; in[0][1] = i <<< 16; in[0][2] = i <<< 16; start = 0;
			@(posedge clock);
		end
		in[0][0] = 0; in[0][1] = 0; in[0][2] = 0;
		@(posedge clock);
		for (int i = 1; i < 56; i++) begin
			in[0][0] = i <<< 16; in[0][1] = i <<< 16; in[0][2] = i <<< 16; start = 0;
			@(posedge clock);
		end


		in[0][0] = 0; in[0][1] = 0; in[0][2] = 0;
		@(posedge clock);
		for (int i = 1; i < 56; i++) begin
			in[0][0] = i <<< 16; in[0][1] = i <<< 16; in[0][2] = i <<< 16; start = 0;
			@(posedge clock);
		end

		in[0][0] = 0; in[0][1] = 0; in[0][2] = 0;
		@(posedge clock);
		for (int i = 1; i < 56; i++) begin
			in[0][0] = i <<< 16; in[0][1] = i <<< 16; in[0][2] = i <<< 16; start = 0;
			@(posedge clock);
		end
*/
		@(posedge clock);
		$finish;
	end
endmodule

