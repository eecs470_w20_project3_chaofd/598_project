#####################################################
#Read design data & technology
#####################################################

set CURRENT_PATH [pwd]
set TOP_DESIGN toplevel

## Add libraries below:
set search_path [ list "../"  "/afs/umich.edu/class/eecs470/lib/synopsys/" ]
set target_library "lec25dscc25_TT.db"
set LINK_PATH [concat  "*" $target_library]

## Replace with your design names:
set SDC_PATH      "/home/chaofd/598/598_project/"
set STRIP_PATH    toplevel_test/dut_top

set ACTIVITY_FILE ../toplevel_test.vcd

######## Timing Sections ########
set START_TIME 10000
set	END_TIME 831000
##### replace start and end time in pp.tcl
set fp    [open pp.tcl r]
set newfp [open pp.tset.tcl w]
set map {}
lappend map {@START_TIME} $START_TIME
lappend map {@END_TIME} $END_TIME
while {[gets $fp line] >= 0} { 
	set  newline [string map $map $line] 
	puts $newfp $newline 
};
close $fp
close $newfp