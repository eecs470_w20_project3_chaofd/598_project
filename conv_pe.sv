`timescale 1us/100ns

module conv_pe(
    input clock,
    input reset,
    input start,
    input new_round,
    input signed [2:0][2:0][31:0] weight_in ,
    input signed [2:0][2:0][31:0] in,
    output logic start_carry,
    output logic new_round_carry,
    output logic signed [15:0] sum_out,
    output logic signed [2:0][2:0][31:0] in_carry 
    );


logic signed [15:0] sum_out_pre;
logic signed [47:0]  multiplication_result;

logic start_carry_pre;


always_comb begin
    if (start) begin   
        start_carry_pre = 1'b1;
    end
    else begin
        start_carry_pre = 1'b0;
    end
end

always_comb begin
    if (reset) begin
        sum_out_pre = sum_out;
        multiplication_result = 0;
    end
    else begin
        if (start) begin
            multiplication_result = in[0][0] * weight_in[0][0] + in[0][1] * weight_in[0][1] + in[0][2] * weight_in[0][2] + 
                                in[1][0] * weight_in[1][0] + in[1][1] * weight_in[1][1] + in[1][2] * weight_in[1][2] +
                                in[2][0] * weight_in[2][0] + in[2][1] * weight_in[2][1] + in[2][2] * weight_in[2][2];
            sum_out_pre = sum_out + (multiplication_result[31:16]);
        end
        else begin
            if (new_round) begin
                sum_out_pre = 0;
            end 
            else begin
                sum_out_pre = sum_out;
            end        
        end
    end
end
    
always_ff @(posedge clock) begin
    if (reset) begin
        sum_out <= 0;
        start_carry <= 0;
        new_round_carry <= 0;
        for (int i = 0; i < 3; i++) begin
            in_carry[i] <= 96'd0;
        end
    end
    else begin
        start_carry <= start_carry_pre;
        new_round_carry <= new_round;
        in_carry <= in;
        sum_out <= sum_out_pre; // to cancel out fixed number R
        
    end
end
endmodule